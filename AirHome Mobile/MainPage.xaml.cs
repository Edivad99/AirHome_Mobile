﻿using AirHome_Mobile.Class;
using AirHome_Mobile.Schermate;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x410

namespace AirHome_Mobile
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
            this.InitializeComponent();
        }
        public SeriesCollection SeriesCollectionTemp { get; set; }
        public string[] LabelsTemp { get; set; }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values["dominio"] == null)
            {
                Frame.Navigate(typeof(Settings));
            }
            else
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    CaricaValori();
                }
            }
        }

        private void AppBarToggleButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Settings));
        }

        

        private async void CaricaValori()
        {
            AirHomeData dt = new AirHomeData();
            var dentro = await dt.GetLast20Records();
            var fuori = await dt.MeteoEsterno();
            bool errore = false;

            if (dentro == null || fuori == null)
            {
                errore = true;
            }

            if(!errore)
            {
                var supp = dentro.Last();
                Interno.DataContext = supp;
                Esterno.DataContext = fuori;
                Esterno.Header = "Fuori Casa a " + fuori.Citta;
            }
            else
            {
                var d = new MessageDialog("Server non raggiungibile").ShowAsync();
            }
        }

        private void RiCaricaValori()
        {
            PushNotification.InitializeNotifications();
            CaricaValori();
        }

        private void AppBarToggleButton_Click_1(object sender, RoutedEventArgs e)
        {
            RiCaricaValori();
        }

        private void Rectangle_Temperature(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Temperatura));
        }

        private void Rectangle_Humidity(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Umidità));
        }

        private void Rectangle_Pressure(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Pressione));
        }

        private void Rectangle_CO2(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(CO2));
        }

        private void Rectangle_Search(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Ricerca));
        }
    }
}
