﻿using AirHome_Mobile.Class;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome_Mobile.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Umidità : Page
    {
        public Umidità()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            this.InitializeComponent();
            GraficoLast20();
            Crea_Grafico24H();
            Statistica();
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public SeriesCollection SeriesCollection24h { get; set; }
        public string[] Labels24h { get; set; }

        public async void GraficoLast20()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Humidity();

            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Umidita);
                    ora[i] = result[i].Differenza();
                }

                SeriesCollection = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Umidità",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore linea
                    }
                };

                Labels = ora;
                UmidChart.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
        }

        public async void Crea_Grafico24H()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.Humidity24H();
            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Umidita);
                    ora[i] = "Ore: " + result[i].Ora.Hour;
                }

                SeriesCollection24h = new SeriesCollection
                {
                    new ColumnSeries
                    {
                        Title = "Umidità",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore linea
                    }
                };

                Labels24h = ora;
                h24.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }

        }

        public async void Statistica()
        {
            AirHomeData data = new AirHomeData();
            var x = await data.GetStatisticaUmidita();
            if (x != null)
            {
                if (x.Umid_media_precedente != null)
                {

                    Umedpre.Text = "Umidità media giorno precedente: " + x.Umid_media_precedente.ToString().Replace('.', ',') + "%";
                }
                else
                    Umedpre.Text = "Umidità media giorno precedente: N/D";

                Umax.Text = "Umidità massima: " + x.Umid_max + "%";
                Umin.Text = "Umidità minima: " + x.Umid_min + "%";
                Umed.Text = "Umidità media: " + x.Umid_media + "%";
            }
        }
    }
}
