﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.Net;
using Windows.UI.Popups;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using AirHome_Mobile.Class;
using Windows.Graphics.Display;
// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome_Mobile.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Settings : Page
    {
        ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
        IPAddress ipAddress;

        public Settings()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
            this.InitializeComponent();

            if (localSettings.Values["dominio"] == null)
            {
                txt_ip.Text = "http://edivad.myftp.org";
            }
            else
            {
                txt_ip.Text = localSettings.Values["dominio"].ToString();
            }

            if (localSettings.Values["silenzioso"].ToString() == "NO")
            {
                SilenziaSlider.IsEnabled = true;
                ConfermaButton.IsEnabled = true;
                ResettaButton.IsEnabled = false;
            }
            else
            {
                string[] tempo = localSettings.Values["silenzioso"].ToString().Replace(" del ", "#").Split('#');
                string[] ora = tempo[0].Split(':');
                string[] data = tempo[1].Split('/');
                DateTime adesso = DateTime.Now;
                DateTime temp = new DateTime(int.Parse(data[2]), int.Parse(data[1]), int.Parse(data[0]), int.Parse(ora[0]), int.Parse(ora[1]), int.Parse(ora[2]));
                TimeSpan diff = temp.Subtract(adesso);

                if (temp.CompareTo(adesso) > 0)
                {
                    SilenziaSlider.IsEnabled = false;
                    ConfermaButton.IsEnabled = false;
                    ResettaButton.IsEnabled = true;
                    mostraValore.Text = "Notifiche silenziate fino alle " + localSettings.Values["silenzioso"];
                }
                else
                {
                    SilenziaSlider.IsEnabled = true;
                    ConfermaButton.IsEnabled = true;
                    ResettaButton.IsEnabled = false;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Progress.IsActive = true;
            if (isSite(txt_ip.Text))
            {
                localSettings.Values["dominio"] = txt_ip.Text;
                PushNotification.InitializeNotifications();
            }
            else
            {
                var d = new MessageDialog("Sito non valido!").ShowAsync();
            }
            Progress.IsActive = false;
        }

        public bool isSite(string input)
        {
            bool result = false;
            string pattern = @"^(http\:\/\/|https\:\/\/)?([a-z][a-z\-]*\.)+[a-z][a-z\-]*$";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(input);
            if (matches.Count > 0)
            {
                result = true;
            }
            return result;
        }

        /*
         * Notifiche 
         */

        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (SilenziaSlider.Value == 0)
            {
                mostraValore.Text = "Silenzia notifiche";
                ConfermaButton.IsEnabled = false;
            }
            else if (SilenziaSlider.Value == 1)
            {
                mostraValore.Text = "Silenzia notifiche per 1 ora";
                ConfermaButton.IsEnabled = true;
            }
            else
            {
                mostraValore.Text = String.Format("Silenzia notifiche per {0} ore", SilenziaSlider.Value);
                ConfermaButton.IsEnabled = true;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Silenzia();
        }

        public async void Silenzia()
        {
            Progress.IsActive = true;
            int oreAggiunte = Convert.ToInt32(SilenziaSlider.Value);
            if (oreAggiunte > 0)
            {
                DateTime adesso = DateTime.Now;
                adesso = adesso.AddHours(oreAggiunte);
                string dataDB = String.Format("{0}-{1}-{2} {3}-{4}-00", adesso.Year, adesso.Month, adesso.Day, adesso.Hour, adesso.Minute);
                string dataUSR = String.Format("{3}:{4}:00 del {0}/{1}/{2}", adesso.Day, adesso.Month, adesso.Year, adesso.Hour, adesso.Minute);
                AirHomeData dt = new AirHomeData();
                string risposta = await dt.SilenziaNotifiche(dataDB);
                if (risposta == "OK")
                {
                    mostraValore.Text = "Notifiche silenziate fino alle " + dataUSR;
                    ConfermaButton.IsEnabled = false;
                    SilenziaSlider.IsEnabled = false;
                    ResettaButton.IsEnabled = true;
                    localSettings.Values["silenzioso"] = dataUSR;
                }
            }
            Progress.IsActive = false;
        }

        public async void Resetta()
        {
            Progress.IsActive = true;
            DateTime adesso = DateTime.Now;
            string dataDB = String.Format("{0}-{1}-{2} {3}-{4}-00", adesso.Year, adesso.Month, adesso.Day, adesso.Hour, adesso.Minute);
            AirHomeData dt = new AirHomeData();
            string risposta = await dt.SilenziaNotifiche(dataDB);
            if (risposta == "OK")
            {
                mostraValore.Text = "Silenzia notifiche";
                ConfermaButton.IsEnabled = false;
                SilenziaSlider.IsEnabled = true;
                SilenziaSlider.Value = 0;
                ResettaButton.IsEnabled = false;
                localSettings.Values["silenzioso"] = "NO";
            }
            Progress.IsActive = false;
        }

        private void ConfermaButton_Click(object sender, RoutedEventArgs e)
        {
            Resetta();
        }
    }
}
